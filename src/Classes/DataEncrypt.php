<?php

namespace  Smallsha\Classes;

class OrderBase64
{
    const IV = 'caJV5aV0GY!dbUb0';
    const CHAR = 'qXB&QgIUFzpd&@KO5dR$K210TOKkFvwE%F';

    public static function encode($string): string
    {
        return str_replace(['+', '/'], ['-', '_',], base64_encode(openssl_encrypt($string, "AES-128-CBC", self::CHAR, true, self::IV)));
    }

    public static function decode($string): string
    {
        return openssl_decrypt(base64_decode(str_replace(['-', '_'], ['+', '/'], $string)), "AES-128-CBC", self::CHAR, true, self::IV);
    }

    public static function encodeByKey($string, $char, $iv): string
    {
        return str_replace(['+', '/'], ['-', '_',], base64_encode(openssl_encrypt($string, "AES-128-CBC", $char, true, $iv)));
    }
}